package com.pens.favian.intent1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView txtBat;
    private BroadcastReceiver mBatInfoRec = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra("level", 0);
            txtBat.setText(String.valueOf(level)+ "%");
        }
    };

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_main);
        txtBat = (TextView)this.findViewById(R.id.txtView);
        this.registerReceiver(this.mBatInfoRec, new IntentFilter
                (Intent.ACTION_BATTERY_CHANGED));
    }
}
